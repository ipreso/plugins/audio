<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Actions_Plugin_AudioTable extends Zend_Db_Table
{
    protected $_name = 'Actions_audio';

    public function get ($hash)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        return (array ('output'     => $row ['output'],
                       'modified'   => $row ['modified']));
    }

    public function set ($hash, $output)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['output']    = $output;
                $row ['modified']  = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'output'    => $output));

        return ($hashConfirm);
    }

    public function setOutput ($hash, $output)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['output']    = $output;
                $row ['modified']  = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        return (NULL);
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        
        return (true);
    }
}

<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/AudioTable.php';

class Actions_Plugin_Audio extends Actions_Plugin_Skeleton
{
    public function __construct ()
    {
        $this->_name        = 'Audio';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Audio'));
    }

    public function getContextProperties ($context)
    {
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                return (NULL);
                break;
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                return (NULL);
        }

        return ($properties->getHTMLValues ());
    }

    protected function getBoxProperties ($id)
    {
        $table = new Actions_Plugin_AudioTable ();
        $result = $table->get ("$id");
        if ($result == false)
        {
            // Create default values
            $table->set ("$id", "hdmi");
            $result = $table->get ("$id");
        }

        $audio_output = $result ['output'];

        $htmlProperties = array ();
        $htmlProperties [] = $this->createSelectProperty (
                                'audio_output',
                                $this->getTranslation ('Audio Output'),
                                array ('hdmi' => $this->getTranslation ('HDMI'),
                                       'jack' => $this->getTranslation ('Jack')),
                                $audio_output);

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("box-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $table = new Actions_Plugin_AudioTable ();

        switch ($param)
        {
            case "audio_output":
                $table->setOutput ($id, $value);
                break;
        }
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $table = new Actions_Plugin_AudioTable ();

        $result = $table->get ($hash);
        if ($result && $result ['modified'] == 1)
        {
            // Send new audio output
            $toSend = array ('cmd'      => "PLUGIN",
                             'params'   => "audio.cmd -o ".$result ['output']);
            $table->setRead ($hash);
            return ($toSend);
        }

        return (false);
    }
}


#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
#
# Variables
#
CONF=/var/lib/iplayer/cmd/audio.conf

###############################################################################
# Log everything as player does
function iInfo
{
    logger -p local4.info -t Audio -- "$@"
}
function iError
{
    logger -p local4.err -t Audio -- "$@"
}

function usage
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -h                      Display this usage screen"
    echo "  -o <output>             Set the audio Output ('hdmi', 'jack')"
}

function failure
{
    iError "$1"
    exit 1
}

function getMyConfFile ()
{
    echo "$CONF"
}

function parseConf ()
{
    CONFFILE=$( getMyConfFile )
    
    if [ ! -f "$CONFFILE" ];
    then
        OUTPUT="hdmi"
        saveConf
    fi

    OUTPUT=`grep -E '^OUTPUT=' $CONFFILE | head -n1 | sed -r 's/^OUTPUT=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "OUTPUT=${OUTPUT}
" > $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

function configureAudioHDMI
{
    iInfo "Check if HMDI Audio device is available..."
    for INFO in $( find /proc/asound -name info ) ; do
        grep -q "HDMI 0" "${INFO}" || continue
        ID=$( grep -E '^id:' ${INFO} | cut -d' ' -f2- )
        CARD=$( grep -E '^card:' ${INFO} | cut -d' ' -f2- )
        DEVICE=$( grep -E '^device:' ${INFO} | cut -d' ' -f2- )
    done
    if [ -z "${ID}" ] ; then
        iError "No HDMI output found."
        return 1
    else
        iInfo "Found: ${ID} (card ${CARD}, device ${DEVICE})"
    fi

    iInfo "Configure HDMI Audio device as default output..."
    echo "pcm.!default {
    type hw
    card ${CARD}
    device ${DEVICE}
}" | sudo tee /etc/asound.conf &>/dev/null
    return 0
}

function configureAudio ()
{
    case $1 in
        hdmi)
            iInfo "Setting audio output to '${1}'"
            configureAudioHDMI || failure "Unable to configure audio output to ${1}"
            ;;
        jack)
            iInfo "Setting audio output to '${1}'"
            rm -f /etc/asound.conf
            ;;
        *)
            iError "Don't know output '${1}'"
            ;;
    esac
}

###############################################################################

# This script use player permissions, even if launched as root.
if [ "${EUID}" = "0" ];
then
    iInfo "Running $( basename $0 ) as user 'player'..."
    if [[ x = x$@ ]] ; then
        sudo -u player $0
    else
        sudo -u player $0 "$@"
    fi
    exit $?
fi

parseConf

# What should we do ?
flag=
bflag=
while getopts 'ho:' OPTION ; do
    case ${OPTION} in
        h)
            usage $( basename $0 )
            exit 0
            ;;
        o)
            # Configure audio's output
            OUTPUT="${OPTARG}"
            configureAudio "${OUTPUT}"
            saveConf
            ;;
        *)
            usage $( basename $0 )
            exit 2
            ;;
    esac
done

exit 0

# vi:syntax=sh
